from datetime import datetime


def bubble_sort(lista):

    is_sorted = False
    while not is_sorted:
        i = 0
        is_sorted = True
        for element in lista:
            if (i < len(lista) - 1 and element > lista[i+1]):
                is_sorted = False
                temp = lista[i + 1]
                lista[i + 1] = lista[i]
                lista[i] = temp
            i += 1
        print(moja_lista)

    return lista

print("before: " + str(datetime.now().timestamp()))
moja_lista = [2, 456, 78, 67, 1, 0, 3, 1, 50, 34, 7, 28, 33, 4, 555, 43]

moja_lista_posortowana = bubble_sort(moja_lista)

print(f"Posortowana lista: {moja_lista_posortowana}")
print("after: " + str(datetime.now().timestamp()))

